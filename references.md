+ ##  Coding

  + [Google Crashcourse](https://developers.google.com/machine-learning/crash-course)

  + [Coursera Deep Learning](https://github.com/Anacoder1/TensorFlow-in-Practice-deeplearning.ai-Courserahttps://github.com/Anacoder1/TensorFlow-in-Practice-deeplearning.ai-Coursera)

  + [Machine Learning Mastery](https://machinelearningmastery.com/)

  + [Fast AI - Practical Deep Learning for Coders](https://course.fast.ai/#)

  + [OpenAI Spinning Up](https://spinningup.openai.com/)

  + [The Hundred-Page Machine Learning Book](https://github.com/aburkov/theMLbook)

  + [Awesome Datascience](https://github.com/siboehm/awesome-learn-datascience)


##  Courses and Education

+ [MIT: Intro to Deep Learning](http://introtodeeplearning.com/) (brief)
  + [MIT Deep Learning](https://deeplearning.mit.edu/) (full)
  + [Stanford CS221: Artificial Intelligence: Principles and Techniques](https://stanford-cs221.github.io/autumn2020/)
  + [Stanford CS231n: Convolutional Neural Networks for Visual Recognition](http://cs231n.stanford.edu/2019/)
  + [Stanford CS234: Reinforcement learning](https://web.stanford.edu/class/cs234/index.html)
  + [Berkley CS285: Deep Reinforcement Learning](http://rail.eecs.berkeley.edu/deeprlcourse/)
  + [DeepMind at UCL: Deep Learning Lectures ](https://www.youtube.com/watch?v=7R52wiUgxZI)
  + [Google AI Education](https://ai.google/education/)
  + [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/)
  + [Towards Data Science](https://towardsdatascience.com)
  + [Mathematics for Machine Learning](https://mml-book.github.io/)
  + [Data Driven Control with Machine Learning](https://www.youtube.com/playlist?app=desktop&list=PLMrJAkhIeNNQkv98vuPjO2X2qJO_UPeWR)
  + [Artifical Neural Networks: Deep Learning and Reinforcement Learning at EPFL](https://lcnwww.epfl.ch/gerstner/VideoLecturesANN-Gerstner.html)


  ## Books

  + Artifical Intelligence: A Modern Approach
  + Rebooting AI: Building Artifical Inteligence We Can Trust by Gary Marcus and Ernest Davis
  + Artifical Intelligence - A Guide for Thinking Humans by Melanie Mitchell
  + Deep Learning with Python by François Chollet
  + Machine Learning Learning by Andrew Ng
  + Deep Learning by Ian Goodfellow, Yoshua bengio, Aaron Courveville
  + [The Hundred-Page Machine Learning Book](https://github.com/aburkov/theMLbook)
  + Deep Reinforcement Learning in Action by Alexander Zai and Brandon Brown
  + Grokking Deep Reinforcement Learning  by Miguel Morales


  ## Community and Conferences

  + [https://paperswithcode.com/](https://paperswithcode.com/)
  + [https://openreview.net/group?id=ICLR.cc/2020/Conference](https://openreview.net/group?id=ICLR.cc/2020/Conference)
  + [https://nips.cc/](https://nips.cc/)
  + [https://icml.cc/](https://icml.cc/)
  + [https://distill.pub/](https://distill.pub/)


  ## Other

  + [Topics in ML, tutorials and other resources](https://medium.com/machine-learning-in-practice/over-200-of-the-best-machine-learning-nlp-and-python-tutorials-2018-edition-dd8cf53cb7dc)
  + [Awesome RL](https://github.com/aikorea/awesome-rl)


  ## Reinforcement Learning

  + [Introduction to Reinforcement Learning with David Silver](https://deepmind.com/learning-resources/-introduction-reinforcement-learning-david-silver)
  + [A (Long) Peek into Reinforcement Learning](https://lilianweng.github.io/lil-log/2018/02/19/a-long-peek-into-reinforcement-learning.html)
  + [Reinforcement Learning: An Introduction by Richard Sutton](http://incompleteideas.net/book/RLbook2020.pdf)

  ## Deep Learning

  + [Neural Networks](https://cs.stanford.edu/people/eroberts/courses/soco/projects/neural-networks/Comparison/topdown.html)
  + [Deep Learning](https://www.deeplearningbook.org/)
  + [Deep Learning with Python](https://www.manning.com/books/deep-learning-with-python)

  ## Deep RL and Algorithms

  + [A Brief Survey of Deep Reinforcement Learning](https://arxiv.org/pdf/1708.05866.pdf)
  + [An Introduction to Deep Reinforcement Learning](https://arxiv.org/pdf/1811.12560.pdf)
  + [Deep Reinforcement Learning: CS285 at UC Berkley](http://rail.eecs.berkeley.edu/deeprlcourse/)
  + [Exploration Strategies in Deep RL](https://lilianweng.github.io/lil-log/2020/06/07/exploration-strategies-in-deep-reinforcement-learning.html)
  + [Policy Gradient Algorithms](https://lilianweng.github.io/lil-log/2018/04/08/policy-gradient-algorithms.html)
  + [Spinning up](https://spinningup.openai.com/en/latest/)
  + [Intel Coach](https://intellabs.github.io/coach/)

  ## Safety

  + [AI Research Considerations for Human Existential Safety (ARCHES)](https://arxiv.org/abs/2006.04948)
  + [Building safe AI](https://medium.com/@deepmindsafetyresearch/building-safe-artificial-intelligence-52f5f75058f1)
  + [Concrete Problems in AI Safety](https://arxiv.org/abs/1606.06565)
  + [Towards Safe RL](https://medium.com/@harshitsikchi/towards-safe-reinforcement-learning-88b7caa5702e)
  + [AI Paradigms and AI Safety: Mapping Artefacts and Techniques to Safety Issue](https://ecai2020.eu/papers/1364_paper.pdf)
  + [Safe Reinforcement Learning Papers](https://github.com/vermouth1992/safe_rl_papers)
  + [Safe reinforcement learning with stability guarantees](https://github.com/befelix/safe_learning)
  + [Safe Exploration with MPC and Gaussian process models](https://github.com/befelix/safe-exploration)
  + All of Andreas Krause lectures and talks
    + [Andreas Krause (ETH Zuerich): "Safe Exploration in Reinforcement Learning"](https://www.youtube.com/watch?v=5vMyz4HWYfw)
    + [CoRL 2020 Tutorial 2 - Andreas Krause](https://www.youtube.com/watch?v=QBxE59pJu8g)
    + [Fall 2020 LIDS Seminar — Andreas Krause (ETH Zürich)](https://www.youtube.com/watch?v=GdGrPPXih4A)
  + [A Visual Exploration of Gaussian Processes](https://distill.pub/2019/visual-exploration-gaussian-processes/)
  + [Bayeseian Optimisation](https://distill.pub/2020/bayesian-optimization/)
  + [Safe and Robust Reinforcement Learning with Neural Netwrok Policies](file:///Users/dmelichar/gdrive/journal/Projects/thesis/ba/1128181055-MIT.pdf)
  + [AI Safety Research](https://futureoflife.org/ai-safety-research/?cn-reloaded=1) (list)
  + [AI Safety @ Stanford](http://aisafety.stanford.edu/) (list, more papers)
  + [10 Reasons to Ignore AI Safety](https://www.youtube.com/watch?v=9i1WlcCudpU) (video)
  + [Safe Machine Learning](https://slideslive.com/38917412/safe-machine-learning)

  ## Challenges

  + [Deep RP Doesn't Work Yet](https://www.alexirpan.com/2018/02/14/rl-hard.html)

  + [Challenges of real world RL](https://blog.acolyer.org/2020/01/13/challenges-of-real-world-rl/)



  ## Artifical Intelligence and Machine Learning

  + [Artificial Intelligence on Stanford Encylopedia of Philosophy](https://plato.stanford.edu/entries/artificial-intelligence/)
  + [Artifical Intelligence: A Modern Approach](http://aima.cs.berkeley.edu/)
  + [Machine Learning 101](https://docs.google.com/presentation/d/1kSuQyW5DTnkVaZEjGYCkfOxvzCqGEFzWBy4e9Uedd9k/preview?imm_mid=0f9b7e&cmp=em-data-na-na-newsltr_20171213#slide=id.g168a3288f7_0_58)


  ## Frameworks

  + [A Comparison of Reinforcement Learning Frameworks](https://winderresearch.com/a-comparison-of-reinforcement-learning-frameworks-dopamine-rllib-keras-rl-coach-trfl-tensorforce-coach-and-more/)
  + [Tonic RL library](https://github.com/fabiopardo/tonic)
  + [Garage](https://github.com/rlworkgroup/garage)
  + [Stable Baselines](https://github.com/hill-a/stable-baselines)
  + [Tensorforce](https://github.com/tensorforce/tensorforce)
  + [Coach](https://github.com/IntelLabs/coach)
  + [ACME](https://github.com/deepmind/acme)
  + [VSRL](https://github.com/IBM/vsrl-framework)

  ## Environments

  + [Drone Delivery Environment](https://github.com/IBM/vsrl-examples/blob/master/examples/EnvBuild/DroneDelivery.md)
  + [Unity Drone Simulator](https://github.com/UAVs-at-Berkeley/UnityDroneSim)
  + [AirSim](https://github.com/microsoft/AirSim)

  ## Lists

  + [Awesome Artificial  Intelligence](https://github.com/owainlewis/awesome-artificial-intelligence)

  + [Applied ML](https://github.com/eugeneyan/applied-ml)

  + [Awesome Real World RL](https://github.com/ugurkanates/awesome-real-world-rl)

  + [Awesome Reinforcement Learning](https://github.com/aikorea/awesome-rl)

  + [Eleurent's Awesome PhD Bibliography](https://github.com/eleurent/phd-bibliography)

  ## Community and conferences

  + [https://paperswithcode.com/](https://paperswithcode.com/)
  + [https://openreview.net/group?id=ICLR.cc/2020/Conference](https://openreview.net/group?id=ICLR.cc/2020/Conference)
  + [https://nips.cc/](https://nips.cc/)
  + [https://icml.cc/](https://icml.cc/)
  + https://distill.pub/


