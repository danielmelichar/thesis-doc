# Bachelor Thesis

This is the content of my bachelor paper, and some ressources that helped me
during the writing process.

Chapters
--------

- [x] Abstract
- [x] Kurzfassung
- [x] Introduction
  - [x] Problem statement
  - [x] Contribution and related work
- [x] Preliminaries
  - [x] Reinforcement Learning
  - [x] Deep Learning
- [x] Algorithms
- [x] Challenges
- [x] Experiments
- [x] Conclusions

Writing
-------

The **WISDOM Acrostic** from the pragrmatic programmer

    What do you want them to learn?
    What is their interest in what you've got to say?
    How sophisticated are they?
    How much detail do they want?
    Whom do you want to own the information?
    How can you motivate them to listen to you?

Checklists and guides
---------------------

- [ ] [How to write better essays](http://medicine.kaums.ac.ir/UploadedFiles/Files/How_Write_Better_Essays.pdf)
- [ ] Writing guide from [Göschka/Teschl](https://cis.technikum-wien.at/cms/dms.php?id=282) and [Widhalm](https://moodle.technikum-wien.at/pluginfile.php/664158/mod_page/content/11/WissenschaftlichesArbeiten.pdf)
- [ ] Writing guide from [Millinger](https://docs.google.com/document/d/1aqLpvxUeR3HqrPfU-P9D30C_VKwT7gsZ0g2SmAcFpn4/edit)
- [ ] [Writing guide from Peterson](https://docs.google.com/viewer?url=https%3A%2F%2Fjordanbpeterson.com%2Fwp-content%2Fuploads%2F2018%2F02%2FEssay_Writing_Guide.docx)
- [ ] [Infokurs ](https://moodle.technikum-wien.at/mod/page/view.php?id=420483)
  - [ ] [Presentation](https://moodle.technikum-wien.at/pluginfile.php/664158/mod_page/content/16/BIC6-InfoBp.pdf?time=1614076337272)
  - [ ] [Bewertung](https://cis.technikum-wien.at/cms/dms.php?id=1371)
