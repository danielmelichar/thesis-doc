\hypertarget{algorithms}{%
\chapter{Algorithms}\label{algorithms}}


\colorbox{lightgray}{
  \parbox{\textwidth}{
  \textbf{This chapter covers:}

  \begin{itemize}
  \tightlist
  \item Value-based methods and the DQN algorithm
  \item Policy gradient methods and the REINFORCE algorithm
  \item Actor-Citic methods and the A3C/A2C algorithm
  \end{itemize}
}}

\hfill \break

So far it has been stated that the goal of RL is to find the optimal
policy \(\pi^*\) for a model of the environment. When applying methods from
DL to approximate the parameters \(\theta\) of the optimal policy \(\pi_\theta^*\).
The qualitative value of a policy is calculated with all successive rewards if the policy is followed (Equation \ref{eq:policygood}).

\begin{equation}\label{eq:policygood}
  \theta^* = arg\max_{\theta} E[G_t].
\end{equation}

There are two central themes in Deep RL algorithms. With value-based methods, the goal
is to find the maximum value of an action or state. With policy gradients, the goal is to learn
a map from state to action. Generally, value-based methods are used when selecting a deterministic
action out of a discrete set of actions, whereas policy-gradient methods can work with a stochastic set
of actions in a continuous action-space \cite{Sutton1998}.

The environment's \emph{state} (\(s \in S\)) is the information that our agent
receives and uses to make a decision about what action ($a \in A$) to take. It could
be raw pixels, sensor data, or any other form of data. The \emph{policy}
\(\pi\) is the strategy our agent follows the successive choice of actions. For
example, in Texas Hold'Em it might be raising every round in case you have a pair of aces.
The \emph{reward} is the feedback the agent gets after taking an action. An action also leads to a new state.
In our example every time you raise or fold, the other players make their move,
and thereby change the game's state. The agent makes a series of actions based on its initial policy \(\pi\) and does
so until the episode ends, and afterwards uses the experience to optimise the actions that yielded the highest value.
The value of a state or an action is given by the weighted sum of rewards while
following a policy. For the value of being in a state the \emph{state-value function} \(V_\pi(s)\) is used with \(s\) being a state.

The expected return $E_\pi$ can be calculated if a policy is followed, referenced in Equation \ref{eq:action-value} and Equation \ref{eq:state-value}.
\citep{weng2018PG} explained how these Equations for action-value and state-value can be unfolded into the immediate reward plus the discounted future values.
These are known as the Bellman equations.

\begin{equation}
\begin{aligned}
V(s) &= \mathbb{E}[G_t \vert S_t = s] \\
&= \mathbb{E} [R_{t+1} + \gamma R_{t+2} + \gamma^2 R_{t+3} + \dots \vert S_t = s] \\
&= \mathbb{E} [R_{t+1} + \gamma (R_{t+2} + \gamma R_{t+3} + \dots) \vert S_t = s] \\
&= \mathbb{E} [R_{t+1} + \gamma G_{t+1} \vert S_t = s] \\
&= \mathbb{E} [R_{t+1} + \gamma V(S_{t+1}) \vert S_t = s]
\end{aligned}
\end{equation}

Similarly, there is an action-value function \(Q_\pi(s, a)\) that
accepts a state \(s \in S\) and an action \(a \in A\) and returns the
value of taking that action given the state.

\begin{equation}
\begin{aligned}
Q(s, a)
&= \mathbb{E} [R_{t+1} + \gamma V(S_{t+1}) \mid S_t = s, A_t = a] \\
&= \mathbb{E} [R_{t+1} + \gamma \mathbb{E}_{a\sim\pi} Q(S_{t+1}, a) \mid S_t = s, A_t = a]
\end{aligned}
\end{equation}

\hypertarget{value-based}{%
\section{Value-based}\label{value-based}}

In 2013, DeepMind published a paper titled \emph{"Playing Atari with Deep
Reinforcement Learning"} \cite{dqn1} that gave rise to a new approach to an old
algorithm. The old algorithm known as Q-learning had been around for decades \cite{qlearning}.
It belongs to a group of classical RL algorithms
called \emph{temporal-difference learning} \cite{Sutton1998}.

The main idea of Q-learning is to predict
the value of a state-action pair, and then compare the prediction to
the actually accumulated rewards at some later time. Occasionally the agent updates its
parameters so that next time better predictions will be made. This allows
the agent to choose the best action for a current state.
Equation \ref{eq:qlearningupdate} describes the rule to update the action-value.

\begin{equation}\label{eq:qlearningupdate}
  Q(S_t, A_t) \leftarrow Q(S_t, A_t) + \alpha (R_{t+1} + \gamma \max_{a \in \mathcal{A}} Q(S_{t+1}, a) - Q(S_t, A_t))
\end{equation}

The parameters \(\gamma\) and \(\alpha\) are called
\emph{hyperparameters}. \(\alpha\) is the \emph{learning
rate} and it controls how quickly the algorithm learns from
each move. The parameter \(\gamma\), the \emph{discount factor}, is
a variable between 0 and 1 that controls how much our agent discounts
future rewards.

The issue with traditional Q-learning is computational limits with larger state-spaces.
Difficulties with memory capacity are present if many states and actions need to be considered,
e.g. autonomous vehicles \cite{Sutton1998}. Instead, one could try to approximate the values.
The \textbf{Deep Q-Network (DQN)} algorithm developed by DeepMind did exactly that.
A naive implementation is described in Algorithm \ref{alg:1}.

\begin{algorithm}
\SetAlgoLined
\emph{Initialize the deep neural network with parameters \(\theta\)}.\;
\emph{Start from an initial state \(s_0\)}.\;

\BlankLine
\For{\(t \in [0, T_\text{total}]\)}{
  Select \(a_{t}\) using a softmax over the Q values \(Q_\theta(s_t, a)\).\;
  Take \(a_t\), observe \(r_{t+1}\) and \(s_{t+1}\).\;
  Update the parameters \(\theta\) by minimizing the loss function with Equation \ref{eq:qlearningupdate};
}
\caption{Naive deep Q-learning. \citepic{qlearning}\label{alg:1}}
\end{algorithm}

Q-learning suffers from instability and divergence when combined with nonlinear
function approximation and bootstrapping (Chapter \ref{challenges}). In another paper
by DeepMind titled \emph{"Human-level control through deep reinforcement learning"}, a descritpion of
how to improve and stabilise a function approximated Q-learning algorithm can be found \cite{Mnih2015}.
Namely, two new methods were introduced: experience replay (Algorithm \ref{alg:2}) and a periodically updated target (Algorithm \ref{alg:3}).\\

\phantomsection{\textbf{Experience Replay}} - The trajectory \(\tau\) of every episode,
that is, everything that happens in an episode \(e = (S_t, A_t, R_t, S_{t+1}, \cdots)\)
is stored in a replay buffer \(D_t = {e_1, \cdots, e_t}\). Random samples from the buffer are
used during the Q-learning update (Equation \ref{eq:qlearningupdate}).

\begin{algorithm}
\SetAlgoLined
In state \(s\), take action \(a\), and observe the new state \(s_{t+1}\)and reward \(r_{t+1}\)\;
Store this as a tuple (\(s\), \(a\), \(s_{t+1}\), \(r_{t+1}\)) in a list\;
Continue to store each experience in this list until you have filled the list to a specific length\;
Once the experience replay memory is filled, randomly select a subset\;
Iterate through this subset and calculate value updates for each subset; store these in a target array (such as \emph{Y}) and store the state \(s\) of each memory in \emph{X}\;
Use \emph{X} and \emph{Y} as a mini-batch for batch training. For subsequent epochs where the array is full, just overwrite old values in your experience replay memory array\;
\caption{Experience Replay algorithm. \citepic{deep-rl-in-action}}\label{alg:2}
\end{algorithm}

\phantomsection{\textbf{Periodically Updated Target}} - Updating on every single step may not proof fruitful.
The rewards may be sparse, most actions may not get any significant reward, and that may cause the
algorithm to be instable. DeepMind proposed a solution \cite{Mnih2015} by duplicating the Q-network.
The regular network is used primarily for action choices. The \emph{target
network} is identical to the primary in the beginning, but lags behind in parameters after some
time. On occasion the target will copy the primary's parameters. The target is used to update the model's parameters.

\begin{algorithm}
  Initialize the Q-network with parameters (weights) \(\theta_1\)\;

  Initialize the target network as a copy of the Q-network \(\theta_1 = \theta_2\)\;

  Use an exploration strategy with the Q-network's parameters
  \(\theta_1\) to select action \emph{a}\;

  Observe the reward and new state \(r_{t+1}, s_{t+1}\).\;

  The target network's Q value will be set to \(r_{t+1}\) if the episode
  has just been terminated (i.e., the game was won or lost) or to
  \(r_{t+1} + \gamma max Q_{\theta_{2}(s, a)}\) otherwise\;

  Backpropagate the target network's Q value through the Q-network\;

  Every \emph{C} number of iterations, set \(\theta_1 = \theta_2\)\;
\caption{Periodically Updated Target Algorithm. \citepic{deep-rl-in-action}\label{alg:3}}
\end{algorithm}

For a minimal example of a DQN agent (Attachment \ref{sc:bsp:3}).
There are variations in the family of the value optimization with function approximation agents.
Some aim at solving DQN's problems and include:

\begin{itemize}
\tightlist
\item
  \textbf{Dueling DQN} \cite{wang2016dueling} -
  presents a change in the network structure that makes learning faster.
\item
  \textbf{Double DQN} \cite{bellemare2017distributional} - uses
  two identical neural network models.
\item
  \textbf{Bootstrapped DQN} \cite{bootstrapped} - creates a mask
   in the replay buffer to show relevancy for training.
\end{itemize}

\hfill \break

\phantomsection{\textbf{Conclusions}}

Value-based methods learn an approximated \(Q_{\theta}(s,a)\) for the
optimal action-value function, \(Q^*(s,a)\). Typically they use an
objective function based on the Bellman equation. This optimization is
almost always performed off-policy. The actions taken by the Q-learning agent
are given by Equation \ref{eq:qaction}.

\begin{equation}\label{eq:qaction}
  a(s) = \arg \max_a Q_{\theta}(s,a).
\end{equation}

\hypertarget{policy-gradient}{%
\section{Policy gradient}\label{policy-gradient}}

Chapter \ref{value-based} discussed an off-policy algorithm that approximates the Q function
with a neural network. It outputs an action for a given state. With the Q values, some
strategy to select actions to perform can be formalised. One such strategy
is the epsilon-greedy approach referenced in Chapter \ref{preliminaries}.

The idea behind policy based methods is to skip calculating Q values and strategy selection.
With these methods an action is output directly. A policy gradient method accepts a
state, and returns the probability distribution over the possible actions \(\pi_{\theta}(a|s)\) \cite{Sutton1998}.
Policy gradients \emph{push up} probabilities of actions that lead to high return, and \emph{push down} the
probabilities of actions that lead to lower return \cite{deep-rl-in-action}.

Neural networks need an objective function so that optimisation can be done.
In Q-learning this is done through loss functions such as the \emph{mean squared error}.
This makes Q-learning a form of supervised learning \cite{Sutton1998}. With policy gradient methods, all we
know is the probability distribution of actions, and whether the action led to positive or
negative rewards. The objective function with policy gradients is to maximise a performance measure \(J(\pi)\).
The simplest performance measure is to maximise the expected return
of a parameterized policy \(J(\pi_\theta) = arg \max E_\pi[G]\).
In theory, anything can be used as objective function for \(J(\pi)\), even the state-value
\(V(s)\) or action-value \(Q(s, a)\) function. In practice, different algorithms formulate their own
objective function. The gradient of policy performance \(\nabla_{\theta} J(\pi_{\theta})\) is called
policy gradient, and algorithms that optimize \(\nabla_{\theta} J(\pi_{\theta})\) are called policy gradient algorithms.
A example in code can be found in Attachent \ref{sc:bsp:4}. Equation \ref{eq:wing} contains the formula for
finding the parameters with \(k\) steps using gradient descent \cite{Sutton1998}.

\begin{equation}\label{eq:wing}
  \theta_{k+1} = \theta_k + \alpha \nabla_{\theta} J(\pi_{\theta})
\end{equation}

Policy gradient methods are more useful in the continuous space. With an infinite number of
actions and/or states, estimating value is too expensive computationally. By changing the
parameters \(\theta\) towards the direction suggested by the gradient, the optimal policy parameters
\(\pi_\theta^*\) that yield the highest return can be found \cite{Sutton1998}.

Again, the major advantage is optimising the policy directly. This comes at a price, though.
Policy gradient methods are known to be sample inefficient \cite{Sutton1998}. A lot of training
episodes will be needed until the optimal policy parameters \(\pi_\theta^*\) are found.

A number of policy gradient methods exist. One of those is \textbf{REINFORCE} (Algorithm \ref{alg:4}).
This policy gradient algorithm relies on \(Q_\pi(s,a)\) values to update the policy parameters \(\theta\).

\begin{algorithm}

  Initialize $\theta$ at random\;

  Generate one episode \(S1, A1, R2, S2, A2, …, ST\)\;

  \For{\(t=1, 2, \cdots, T\)}{

    Estimate the the return \(G_t\) since the time step \(t\)\;
    \(\theta \leftarrow \theta + \alpha \gamma^t G_t \nabla \ln \pi(A_t \vert S_t, \theta)\)\;
  }
  \caption{REINFORCE algorithm. \citepic{Sutton1998}\label{alg:4}}
\end{algorithm}

A commonly used variation of REINFORCE is to subtract a baseline value
from the return \(G_t\). That reduces the variance of gradient estimation
while keeping the bias unchanged. For example, the state-value can be subtracted
to focus more on the action-value. For this the advantage function
\(A(s, a) = Q(s, a) - V(s)\) would be used in the gradient descent update.

The REINFORCE algorithm is generally implemented as an episodic algorithm, meaning that
updates to model parameters are done after the agent has completed an entire episode.
The return is computed at the end of the episode. In other words the sum of the
rewards multiplied by their discount rate for each episode. With REINFORCE episodes are generated
from the agent's interaction with the environment, and periodically update the parameters
with those that have the highest advantage.

\hfill \break

\phantomsection{\textbf{Conclusions}}

Policy-gradient methods represent a policy explicitly as \(\pi_{\theta} (a|s)\).
They optimise the parameters \(\theta\) directly by gradient descent on the performance
objective \(J(\pi_{\theta}\)). This optimisation is  performed on-policy. Each update
only uses data collected with the most recent version of the policy.

\hypertarget{actor-critic}{%
\section{Actor-Critic}\label{actor-critic-methods}}

The advantages of value-based methods such as Q-learning lie in the information
they give about states and actions. Methods of this family learn to predict rewards for actions. For
each state they give an action that yields the highest reward.
If we use DQN to play pinball, it will learn to predict the values for the two main
actions - operating the left and right paddles. These values can then be used to
pick a strategy that yields the highest return, generally opting to sequentially chose actions with the
highest value \cite{deep-rl-in-action}.

The advantage of policy gradient methods is that they more directly connected to
the concept of \emph{reinforcement}, as it skips calculating the value of something
and instead finds the strategy (i.e. the policy) to accumulate the highest return - which
is the primary goal of RL. In pinball, if we hit the left paddle and score a bunch of
points, that action will get positively reinforced and will be more likely to be selected the next
time the game is in a similar state \cite{deep-rl-in-action}.

To get the benefits of both value-based and policy gradient, it could be tried to combine them.
This can be done by further reducing the variance of the policy gradient, specifically by using an
additional neural network that has the purpose of estimating the value
function. In this case, a policy network and a value network. The value network updates
the value function parameters \(Q(s, a)\) or \(V(s)\).
The policy network handles updating the policy parameters \(\theta\) in the direction
suggested by the value network \cite{deep-rl-in-action}.

Algorithms of this sort are called \emph{actor-critic}. \emph{Actor}
refers to the policy network where the actions are generated
and \emph{critic} refers to the value network that calculates how good actions are.

An actor-critic method that implements this two neural network approach is
\textbf{Asynchronous Advantage Actor-Critic (A3C)} \cite{mnih2016asynchronous}.
A3C implements a global state-value network \(V(s)\) and multiple actor networks trained in parallel.
The policy networks get synced with the global value parameters from time to time.
The objective function for each actor is to minimise the mean squared error (Algorithm \ref{alg:async}). \textbf{A2C} \cite{mnih2016asynchronous}
is a synchronous, deterministic version of A3C. It introduces a coordinator, whose job it is
to ensure that all agent's episodes are finished before starting new episodes with the updated global value parameters.
While the training is done in parallel, the coordinator ensures that the parameters are synchronous for each agent before
starting a new training run.
%The difference between A3C and A2C is illustrated in Figure \ref{fig:compare}.

\begin{algorithm}
\emph{// Assume global shared $\theta$, $\theta^-$ and counter $T = \theta$.}\;
Initialize thread step counter $t \leftarrow \theta$\;
Initialize target network weights $\theta^- \leftarrow \theta$\;
Initialize network gradients $d\theta \leftarrow \theta$\;
Get initial state $s$\;
\Repeat{$T > T_{max}$}{
  Take action $a$ with $\epsilon$-greedy policy\;
  Receive new state $s'$ and reward $r$\;
  \uIf{$s'$ == terminal}{
    $y = r$\;
    } \uElseIf{$s'$ == non-terminal} {
      $y = r + \gamma max_{a'}Q(s', a', \theta^-)$\;
      }
      Accumulate gradients wrt $\theta$: $d\theta \leftarrow d\theta + \frac{\sigma(y-Q(s,a\theta))^2}{\sigma\theta}$\;
      $s = s'$\;
      $T \leftarrow T + 1$ and $t \leftarrow t + 1$\;
      \If{$T \mod I_{target} == \theta$}{
        Update the target network $\theta^- \leftarrow \theta$\;
        }
        \If{$t \mod I_{AsyncUpd} == \theta$ or $s$ is terminal}{
          Perform asynchronous update of $\theta using d\theta$\;
          Clear gradients $d\theta \leftarrow \theta$\;
          }
          }
\caption{Asynchronous one-step Q-learning for each actor thread. \citepic{mnih2016asynchronous}\label{alg:async}}
\end{algorithm}

% \begin{figure}
% \centering
% \includegraphics[scale=0.4]{/Volumes/data/devel/fhtw/thesis-doc/PICS/A3C_vs_A2C.png}
% \caption{Comparission of Asynchronous Methods algorithm. \citepic{weng2018PG}\label{fig:compare}}
% \end{figure}


