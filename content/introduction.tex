\chapter{Introduction}

One of the distinct human capabilities is decision-making and planning.
We learn from interactions with our surroundings and processing information.
We build cause-effect relationships between our decisions and their impact.
We apply this wisdom to plan ahead.

Human infants are rather unsophisticated compared to other species.
For example, the antelope has all the skills it needs for survival right after being born.
Even if compared to our cousins, the primates, we seem to stand out in our cognitive development \cite{SlowProc}.
An infant has no knowledge of how it should behave, but it does gain experience during its lifetime.
For example the ability to speak. At first a baby will utter random gibberish.
By imitating its parents, receiving feedback for doing well, and given some time,
it will have learn to speak a few recognisable words.
After some years, the baby who could not speak a single word will now be able to articulate complex
ideas. A similar principle applies to appropriate social behaviour, culture, reasoning,
or physical capabilities \cite{eagleman2020livewired}.

The circuitry of our brain is constantly rewritten to tackle challenges, make plans,
leverage opportunities, and make decisions. The brain processes information
from our surroundings, the activities we do, and psychological stress \cite{eagleman2020livewired}.

The study of the brain, the mind and their processes have been part of human thought for ages.
Aristotle and Descartes are among the first \cite{sep-artificial-intelligence} to formalise
ideas on an Artificial Intelligence (abbr. \emph{AI}).

The digital computer and the scientific study of cognition are considered to be the roots of modern AI \cite{sep-artificial-intelligence}.
In 1950 Alan Turing published the paper \emph{Computing Machinery and Intelligence} \cite{10.1093/mind/LIX.236.433}. In the paper,
Turing describes a method to determine a machine's ability to exhibit intelligent behaviour.
In 1956 the graduate mathematicians John McCarthy and Marvin Minsky, as well as their
colleagues Claude Shannon and Nathaniel Rochester organised, \emph{"a 2 month, 10 men study of
artificial intelligence"}. They discussed a set of topics that
still shape the field, such as natural-language processing, neural networks, learning,
abstractions, and creativity \cite{minsky2006proposal}.

Many types of AI systems were developed over decades of research and development.
Machine Learning (abbr. \emph{ML}) is a method within AI that uses tools from statistics
and probability theory \cite{mitchell2019artificial}. It separates itself
from other AI methods by the ability to handle uncertainty and scaling \cite{cs221}.

\hypertarget{Context}{%
\section{Context}\label{Context}}

Deep Learning (abbr. \emph{DL}) is a method within ML where concepts are represented by a
hierarchy of data, with each layer being more sophisticated. The abstractions are almost
always learned with neural networks that also allow for predictions
to be made \cite{Goodfellow-et-al-2016}.

Reinforcement Learning (abbr. \emph{RL}) is a problem setup and a set of solutions within ML.
Fundamentally, RL is a method for developing a decision-making program.
The program, referred to as agent, processes data and proposes cost-effective actions \cite{Sutton1998}.

Deep Reinforcement Learning (abbr. \emph{Deep RL}) is an active research
field combining both approaches with key contributions from decision sciences, economics,
cognitive- and neuropsychology, biology, computer science, and
philosophy \cite{sep-artificial-intelligence} \cite{Sutton1998}.

Central concepts are explained in Chapter \ref{preliminaries} and algorithms in Chapter \ref{algorithms},
two of which were implemented during experimentations described in Chapter \ref{experiments}.
Deep RL is not mature its challenges are described in Chapter \ref{challenges}. These systems are being evaluated for applications
in robotics and industrial automation, as well as for treatment policies in the medical sciences, and financial trading \cite{lorica_2017}.

\hypertarget{problem-statement}{%
\section{Problem statement}\label{problem-statement}}

Autonomous systems are being integrated into the physical world and are applied to
solve its problems, e.g. driving \cite{kiran2021deep}. A combination of ML techniques is
required for autonomous driving. Computer vision may be used to understand the systems' state,
whereas Deep RL may be used to decide and plan the vehicle's acceleration and braking.

Ensuring that these systems act safely is crucial before deployment and acceptance by
society. A safety critical problem of the Deep RL driving agent may be to produce non-harmful
actions if the underlying scene understanding system recognizes a traffic jam
or a child running in front of the car. In this example, the Deep RL agent needs to decide and plan on
its actions if the distance between the car and an object in front reaches a limit.

A conventional programmer may define the problem of braking if a distance limit is reached
as a condition and statement in a form similar to \texttt{if distance is x then break}.
However, this oversimplifies the problem and neglects some aspects. The limit on the distance
between the car and the object in front is not static and is conditionally
depended on the other attributes of the system's state. The limit primarily depends on the
changing factor of friction between tires and the road surface. The limit is not always the same as
there is a difference between the car driving on the highway on a sunny day and driving
in the city during snow. A Deep RL agent avoids a deterministic control flow and favours
a stochastic flow based on the system's state.

Broadly speaking the goal of this research was to determine how the actions of a Deep RL agent
cause no damage. The following questions also directed the author during research.

\begin{itemize}
\tightlist
\item
  How can an automated decision and planning system be developed? How to include a
  safety specification in the RL problem setup?
\item
  What mechanisms can be implemented that minimise negative side effects?
\end{itemize}

\hypertarget{contribution}{%
\section{Contribution}\label{contribution}}

\begin{itemize}
\tightlist
\item Introduction to concepts and methods of Deep RL.
\item Conducted experiments.
  \begin{itemize}
      \item
      Implemented two state-of-the-art algorithms.
      \item
      Showed challenges in reward signal design.
      \item
      Showed use of formal specification to minimise negative side effects.
  \end{itemize}
\end{itemize}

\section{Related work}\label{relatedwork}

For a general introduction into AI and ML, books by \citep{russelNorvig2003} and \citep{rebooting} are related.
The standard textbook of RL is by \citep{Sutton1998},
and for DL is by \citep{Goodfellow-et-al-2016}. Some practically-oriented books were used for experiments, specifically by \citep{deep-rl-in-action}, \citep{neuralnetworksanddeeplearning},
\citep{franoischollet2017learning}, \citep{handson}, and \citep{grokkingdeeprl}.
A number of frameworks exist that implement Deep RL algorithms and which serve as
an alternative to independent developing.

\begin{itemize}
\item Tonic \cite{pardo2020tonic}
\item Garage \cite{garage}
\item Stable Baselines \cite{stable-baselines}
\item VSRL \cite{VSRL2020}
\end{itemize}

