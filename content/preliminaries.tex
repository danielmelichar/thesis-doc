\hypertarget{preliminaries}{%
\chapter{Preliminaries}\label{preliminaries}}

\colorbox{lightgray}{
  \parbox{\textwidth}{
  \textbf{This chapter covers:}

  \begin{itemize}
  \tightlist
  \item Basics of Machine Learning
  \item Paradigm difference between ML and traditional programming
  \end{itemize}
}}

\hfill \break

There has been a long philosophical dispute in computer science about its methodological,
ontological and epistemological alignment. The question of what a program is - a mathematical
object, \emph{just} data, or a mental process - and how to construct it has given rise to many discussions \cite{Eden07a}.

Three major paradigms always seem to manifest. A rationalist approach seeks certain \emph{a priori}
knowledge of correctness by means of deductive reasoning. A technocratic approach seeks
probable \emph{a posteriori} knowledge about reliability with empirical testing. A scientific
approach seeks both \emph{a priori} and \emph{a posteriori} knowledge by combining deductive reasoning
and scientific experimentation \cite{Eden07a}.

Computer and information sciences can be divided into several
theoretical and practical disciplines. Almost all apply different methods of data transformation.
A fundamental pattern exists across all disciplines: a system receives some input, does some form of computation,
and makes the resulting output available.

How the input of a system is transformed into an output and what the necessary computations are
to do so is defined by a set of statements and conditions. Programming is the process of conceptually creating
such statements and conditions, and formalising them as a rule-based algorithm. A simple example of a program, although in
an abstract sense, is a recipe for a three-course dinner. It includes a sequence of cooking steps (the \emph{rules}),
as well as the needed ingredients (the \emph{input}) to produce the dishes (the \emph{output}). Programming is the translation
of problem descriptions and their solutions into a formal set of rules.

What is known as software is a collection of programs and the related data, i.e. a set of formalised
rules in modular components. Today there are two methods of constructing the rules governing
the software's behaviour.

\begin{itemize}
\tightlist
\item
  Deterministic programming (Figure \ref{fig:deterministic-programming}).
\item
  Stochastic programming (Figure \ref{fig:stochastic-programming}).
\end{itemize}

Traditional, deterministic programming relies on the stated
concepts, i.e. defining the control flow to govern the software's
behaviour using rules. The programmer's job is to define the steps that
compute the desired output. A major advantage of this paradigm over
stochastic programming is its transparency. It is always clear why this type of
program acts in a certain way. If a rule is properly defined, the received input is
transformed into the desired output. There are two reasons for a program not
working as intended: a rule does not exist yet, or a rule is poorly defined.

This paradigm works well for most software applications. However, issues emerge the more
complex a program's domain is. The more behaviours that need to be considered, i.e. the number
of rules and their interactions, the higher the probability of unintended behaviour. Another
problem is that by breaking down a problem into parts, the solution space is cut down as well \cite{sep-scientific-reduction}.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.1]{/Volumes/data/devel/fhtw/thesis-doc/PICS/traditional.png}
  \caption{Traditional, deterministic programming}
  \label{fig:deterministic-programming}
\end{figure}


Stochastic programming, as used in ML, takes a different approach. It allows for rules, and
probability distributions of their accuracy to be generated. It allows for the rules to be
generated that will \emph{most likely} produce the desired output. Instead of manually
having to write the control flow using cascading \texttt{if} and \texttt{else} statements,
the program figures out itself what the best sequence of statements and conditions is.
The program \emph{learns} by minimising its estimation errors and increasing accuracy.

Generally speaking some problems in ML can be described as regression
problems. A simple linear regression uses linear functions. With \(k\)
and \(d\) as parameters the program learns to produce a accurate
predictions $y$ for the input \(x\).

\begin{equation}
y = kx + d
\end{equation}

A more complex, and more realistic regression is a multi-variable
equation, where \(w\) represents the coefficients, or \emph{weights} of the
model to be learned.

\begin{equation}
f(x, y, z) = w_1x + w_2y + w_3 z
\end{equation}

The basis of a model is data with features or attributes, the coefficients of the equation. They are the properties of the model to be learned.
For example, a fruit's attributes contain weight and color. The choice of
which data to use is important as it directly affects the quality of the program. The number
of features required for a model to work is referred to as its dimensions. Adding more
dimensions to the model may allow better estimates. But, it may also result in what is
known as overfitting, i.e. accurate results during training but lack of generalisation.

\begin{figure}[h]
  \centering
  \includegraphics[scale=0.1]{/Volumes/data/devel/fhtw/thesis-doc/PICS/stochastic.png}
  \caption{Stochastic programming}
  \label{fig:stochastic-programming}
\end{figure}


\hypertarget{Methods}{%
\chapter{Methods}\label{methods}}

\colorbox{lightgray}{
  \parbox{\textwidth}{
  \textbf{This chapter covers:}

  \begin{itemize}
  \tightlist
  \item Key concepts and terminology of RL and DL
  \item The goal of RL
  \item Function approximation with DL
  \end{itemize}
}}

\hfill \break

A trained ML system can predict, cluster, or classify.
Three major solution approaches exist in ML systems: supervised, unsupervised, and reinforcement.
In supervised learning, what is right and what is wrong is essentially defined by labels.
Equations with $y$ and $x$ are solved for their coefficients. But, just as in
traditional programming, there is no certainty that all the necessary equations are available.
In unsupervised learning, there are no labels, and the goal of the system is to find structure.
It uses tools of statistics to create clusters, encode information, or detect anomalies.
In equations only with $x$ some $y$ is found through experimentation. Reinforcement Learning
is similar in not having a pre-defined structure.
A RL program also learns by experimentation, by trial-and-error.
But, in addition, the program receives a \emph{reward} if it solves the problem well and receives
no reward if it does not. In some applications, combinations of these approaches are found, where unsupervised
and supervised methods are used to understand a scene and RL is used to make decisions and plan \cite{kiran2021deep}.

\hypertarget{reinforcement-learning}{%
\section{Reinforcement Learning}\label{reinforcement-learning}}

RL is about making the most
cost-effective decisions. This is a topic in many other fields. For instance,
control theory studies
ways to control complex systems with known dynamics. Operations research
also studies
decision-making under uncertainty. RL is influenced by these domains,
and contributes to them. Its distinct property is learning through trial-and-error \cite{Sutton1998}.

In RL, computer programs are called agents. An agent is a decision making application.
It may choose between two options. Firstly, it can explore the
environment and try to get new information by learning different states (\(s \in S\)).
The environment is everything outside the agent's direct control, the streams of data that
the agent is processing. The term \emph{state} describes the values of the environment's attributes at some time.
For example, if a robot is trained to pick up a ball, the attributes of the ball such as its weight and location,
as well as the attributes of the robots itself such as arm position and grip strength, are considered to be the environment.
Secondly, the agent can exploit the knowledge it already acquired and give an estimate for the highest value
yielding action (\(a \in A\)). An example of the interplay between agent and environment
in code can be found in Attachment \ref{sc:bsp:2}. Ideally the agent has knowledge
of all states, so that the best decision can always be made. But, that is often not the case. This
\emph{exploration or exploitation} dilemma is a key concept within RL \cite{Sutton1998}. Strategies exist
to solve this trade-off such as \(\epsilon\)-greedy, where an agent does random exploration
\emph{some of the time} with a probability of \(\epsilon\) and takes the optimal action \emph{most of the
time} with a probability of \(1-\epsilon\).

\begin{figure}
\centering
\includegraphics[scale=0.20]{/Volumes/data/devel/fhtw/thesis-doc/PICS/rl.png}
\caption{The Reinforcement Learning Problem. \citefigm{deep-rl-in-action}}
\label{fig:rl-problem}
\end{figure}

The interaction between agent and the environment continue for several cycles. At each step
in the cycle, the agent observes the environment, takes action, receives a new observation,
and a reward signaling the agent's performance (Figure \ref{fig:rl-problem}). The \emph{experience} of the agent is a tuple of state, action, reward and the following state. In this paper, the
reward specifications for giving value to  each state-action pair is defined by a separate system,
a simulated environment. The problem of constructing such a reward is covered  in Chapter \ref{experiments}.
In essence, the reward problem is that the agent may only get what is known as a \emph{sparse reward}, where
the agent does not know which state-action pair lead to receiving a reward.

The environment is changing after every action. How the environment transitions from one state to another is described in its dynamics
model. For the purpose of this paper it is assumed that the environment's dynamics model is also
controlled by the simulation. The trade-off between simulation and the physical world is covered in Chapter \ref{challenges}.

A mathematical way to describe the environment model was developed by Andrei Markov \cite{Sutton1998}.
The Markov decision process (abbr. \emph{MDP}) is a control theoretic solution in which the agent knows
all states. For example, a decision making process in control theory is dynamic programming \cite{Sutton1998}.
The MDP is useful to conceptually understand the environment's model, and what the agent is actually learning.

Figure \ref{mdp} inspired by \citep{deep-rl-in-action} shows the likelihood (\(p\)) of one state (\(s \in S\))
transitioning to another state if an action (\(a \in A\)) is taken, and the reward (\(r \in R\))
given for that transition. It depicts the decision process of taking care of an infant.
If the baby is crying, it can be given food or not and with some probability the
baby will transition into a new state, and a reward will be received.

The agent updates its internal reward-getting policy \(\pi(s)\). With a policy, the agent can predict the reward
it will get for future states and actions. One such prediction is known as the state-value
function \(V(s)\) and another is known as the action-value function \(Q(s, a)\).
The goal of RL is to find the best action to take in each
state, i.e. the optimal policy \(\pi^*\). To evaluate how well an agent
is doing, the return is used to estimate the state-value and action-value.
In the beginning the agent will take arbitrary actions without much value,
but after many iterations it will converge towards the optimal policy.

\begin{figure}[!ht]
  \centering
  \includegraphics[scale=0.15]{/Volumes/data/devel/fhtw/thesis-doc/PICS/mdp.png}
  \caption{A markov decision process}
  \label{mdp}
\end{figure}

\hfill \break

\phantomsection{\textbf{Conclusions}}

An agent is set out into an environment with a the goal of accumulating
the highest possible reward. It does so by taking actions within the environment's possibilities \((a \in A)\). Taking actions
also changes the environment into a new state (\(s \in S\)) as is
defined by the environment's model. The agent receives a reward
(\(r \in R\)) measured by how well it did after each action. The loop of taking
an action, getting a reward, observing the new state and taking
another action is the agent's learning process.

\begin{itemize}
\item
  \textbf{Agent} - A cost-effective decision making program.
\item
  \textbf{Environment} - Various data sources.
\item
  \textbf{Reward and return} - The feedback for each action
  \(r_t = R(s_t, a_t, s_{t+1})\), and the sum of all.
  \begin{equation}
    G_t = r_{t+1} + \gamma r_{t+1} + \dotsb = \sum_{t}^{T} \gamma^t r_t
  \end{equation}
 \item
  \textbf{Discount factor} - In an unknown environment, the agent does not have
  complete knowledge of its model. To consider that rewards at a later time do not
  influence current decisions, a factor \(\gamma\) is
  introduced. Getting a reward now may be better than getting a reward later.
\item
  \textbf{State} \textbf{or observation} - The attributes of the
  environment or the part of the environment that the agent can see
  (\(s \in S\)).
\item
  \textbf{Action space} - What the agent is allowed to do (\(a \in A\)).
\item
  \textbf{Trajectories} - Frequently known as episodes or rollouts.
  Pairs of states, actions, and rewards over steps in time.
  \begin{equation}
    \tau = (S_1, A_1, R_1, S_2, A_2, ... S_T)
  \end{equation}
\item
  \textbf{Model-based or Model-free} - Whether the dynamics of the
  environment are known to the agent or not.
\item
  \textbf{Transition probabilities} - The probability that one state
  becomes another after an action has been taken.
  \begin{equation}
    P(s', r | s, a) = p(S_{t+1} = s', R_{t+1} = r | S_t = s, A_t = a)
  \end{equation}
\item
\textbf{Deterministic or stochastic policy} - A strategy of actions to take. It can be certain
  \(a_t = \pi(s_t)\) or approximate \(a_t \approx \pi(*|s_t)\).
\item
  \textbf{State-Value function} - The expected return of the following states if a policy is continued.
  \begin{equation}\label{eq:state-value}
    V_\pi(s) = E_\pi[G_t | S_t = s]
  \end{equation}
\item
  \textbf{Action-value function} - The expected return of an action if a policy is continued.
  \begin{equation}\label{eq:action-value}
    Q_\pi(s, a) = E_\pi[G_t | S_t = s, A_t = a]
  \end{equation}
\item
  \textbf{Advantage function} - It may be useful to not estimate the
  value of an action in an absolute sense, but rather how much better
  it is than the average of the existing trajectory.
  \begin{equation}
    A_\pi(s, a) = Q_\pi(s, a) - V_\pi(s)
  \end{equation}
\item
  \textbf{On-policy or off-policy} - During the training phase, the agent does not know
  the optimal policy \(\pi^*\) yet, but still needs to make action choices.  With each
  episode the agent converges more toward the optimal policy. But, during training an off-policy
  algorithm will not use the current estimate for the optimal policy (e.g. as epsilon-greedy).
  An on-policy algorithm will use the current estimate for the optimal policy.
\end{itemize}

\hypertarget{neural-networks-and-deep-learning}{%
\section{Neural Networks and Deep Learning}\label{neural-networks-and-deep-learning}}

The brain is made up of millions of cells called neurons. Each cell has long wire-like extensions
to called axons that connect to other cells through a synapse. They can be thought of as biological information processing units.
These cells communicate to another by sending out short electrical pulses, called action
potentials or spikes. The neural network of the brain is a densely connected network of cells.
A neuron has a threshold of necessary electrical charge until it is activated \cite{Goodfellow-et-al-2016}.

Artificial neural networks (for brevity referred to as neural nets)
are simplified mathematical models of neurons and synapses. This chapter only describes feed
forward networks, recurrent neural nets are not within this paper's scope. A simple multi-layer neural
network includes four major components:

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}
\item
  The value of inputs denoted by \(x_k\)
\item
  The weight of a synapse denoted by \(w_{ik}\)
\item
  The threshold or activation function denoted by \(g\)
\item
  And the output for the receiving neuron is given by \(x_i = g(\sum_{k}{w_{ik}x_k})\)
\end{enumerate}

The perceptron is an early neuron model \cite{neuralnetworksanddeeplearning}.
Although more modern versions exist, starting with this simple one is useful for understanding.
Perceptrons also have an input point mapped to an output point. Multiple perceptrons can be linked so that a connected network forms.
A parameter called \emph{weight} between the connections sets the
importance of the connection. Perceptrons receive a binary input \([0, 1]\) and produce a binary output.
A perceptron may produce such a binary output only if it is active. It becomes active once the parameter's
of its threshold function reach a certain value. The step function is typically used with perceptrons.
Example code of a perceptron network can be found in Attachment \ref{sc:bsp:1}.

The perceptron's threshold function is the step function. With it a perceptron is either on or off,
and with that logic gates like \texttt{AND} or \texttt{NAND} can be build.  The fundamental principle behind
neural nets is the adjustment of weights and biases until a desired output is computed.
The manual modification of these weights can be tedious with rise in network size, and it does
not allow for \emph{learning}. Additionally small changing may influence the whole more significantly than wanted
since perceptrons are binary. \cite{neuralnetworksanddeeplearning}.

To address these issues other types of neurons were implemented. Fundamentally,
neural networks are made up of vector and matrix operations. The building
blocks of neural networks are the dot-product, elementwise multiplication and addition, as well
as vector-matrix multiplication. Instead of using the notion of
\(x_i = g (\sum_k{w_k x_k})\) a simplified version is using the dot product
\(w \cdot x \equiv \sum_k w_k x_k\), where \(w\) and \(x\) are vectors
whose components are the weights and inputs respectively \cite{neuralnetworksanddeeplearning}.

Sigmoid neurons are an example of other neuron types. Just like a perceptron it has input
values \(x_k\) but instead of being binary, the inputs can be any continuous value.
Also, just like a perceptron, the sigmoid neuron has weights for each input \(w_k\)
and each bias \(b\) \cite{neuralnetworksanddeeplearning}.
A neural network approximates any output by finding the right weights and biases.
The dot-product can be used to express this \(y = w \cdot x \equiv \sum_k w_k x_k\).
A bias \(b\) is added, so that the prediction can be \emph{shifted}. The function then becomes
\(y = w \cdot x + b\). The threshold or activation function \(g\) is now the
sigmoid function \(\sigma\) (Equation \ref{sigmoid}).

\begin{equation}
  \sigma(z) \equiv \frac{1}{1+e^{-z}}
  \label{sigmoid}
\end{equation}

The output of a neuron with inputs \(x_k\), weights \(w_k\) bias \(b\) if equipped with the sigmoid function is Equation \ref{sigmoidneuron}.

\begin{equation}
  x_i =   \frac{1}{1+\exp(-\sum_j w_j x_j-b)}.
  \label{sigmoidneuron}
\end{equation}

There are many other types of neurons with different activation
functions. All are differentiable to compute the loss of a neural net \cite{neuralnetworksanddeeplearning}.

While the threshold or activation function describes how a neuron is activated, the actual
\emph{learning} comes in form of another function. Fundamentally, the goal is to find the
right weights and biases so that the network approximates \(y\) for all inputs \(x\).
A new function is introduced to measure the performance of the network in its approximation.
The loss function (sometimes called cost, objective, or error function) is a way to quantity
the distance between the input and output \cite{Goodfellow-et-al-2016}. A few examples are given in Equations \ref{mae} to \ref{cel}.

\begin{itemize}
\item
  Mean Absolute Error - Also known as L1, minimizes the absolute
  differences between the estimated values and the existing target
  values.
  \begin{equation}
    S = \sum_{i=1}^n |y_i - f(x_i)|
    \label{mae}
  \end{equation}
\item
  Mean Squared Error - Also known as L2, minimizes the squared
  differences between the estimated and existing target values.
  \begin{equation}
    S = \sum_{i=1}^n (y_i - f(x_i))^2
    \label{mse}
  \end{equation}
\item
  Cross Entropy Loss - Also known as log loss, measures performance of
  binary prediction.
  \begin{equation}
    H(p, q) = - \sum_{c=1}^C p(c) \log q(c)
    \label{cel}
  \end{equation}
\end{itemize}

To make the network \emph{learn} it needs to backpropergate, i.e. adjust the weights and reduce the
loss. Gradient descent is an optimisation algorithm used to minimise (or maximise in case of
gradient \emph{ascent}) some function by iteratively moving in the direction of descent until
a minimum of the loss function is found. The idea is to compute the derivative of the
function, which tells us the magnitude and direction of curvature \cite{neuralnetworksanddeeplearning}.
A new point respective to the old point is chosen, its derivative calculated and a step-size
to control how fast we move down on the graph is picked \footnote{For visuals, see \url{https://www.youtube.com/watch?v=IHZwWFHWa-w}}.
Formally this is expressed in code Attachment \ref{alg:4}
or in a mathematical notation Equation \ref{eq:grad}:

\begin{equation}
  x_{new} = x_{old} - \alpha \frac{df}{dx}
  \label{eq:grad}
\end{equation}

A number of different gradient descent algorithms exist and were described by \citep{sebastianruder_2020}.
They include the following:

\begin{itemize}
\tightlist
\item
  Adagrad
\item
  Adam
\item
  Momentum
\item
  SGD
\item
  Newton's Method
\end{itemize}

\textbf{Conclusions}

Deep Neural Networks are a class of ML models composed of multiple neuron layers that
perform matrix-vector multiplication and apply a nonlinear activation
function. The model's learnable parameters are called the weights and biases of the network.
They  can learn any function \cite{neuralnetworksanddeeplearning} and converge toward an
optimal through minimising the distance between predictions and expectations measured by
a loss function. The convergence is done by backpropagation using a gradient descent algorithm.

\begin{itemize}
\item
  \textbf{Neuron} - A neuron takes a group of weighted inputs, applies a
  function, and returns the output.
\item
  \textbf{Synapes} - The links between neurons.
\item
  \textbf{Weights} - The importance of the connection between two
  neurons.
\item
  \textbf{Bias} - An additional constant attached to neurons, added to
  the weighted input before the activation function is applied.
\item
  \textbf{Neural network} - A class of ML algorithms used to model
  patterns and produce predictions. It takes an input, passes it through
  multiple layers of hidden layers, and outputs a prediction based on
  all neurons in the network.
\item
  \textbf{Activation function} - Computes value of the successive hidden
  layers with a non-linear function.
\item
  \textbf{Loss function} - Shows how good the network is by giving
  distance between expectation and estimates.
\item
  \textbf{Backpropagation} - Adjusts each weight in the network in
  proportion to how much it contributes to the overall error. If each weight's error is reduced iteratively,
  a set of weights that produce good predictions is found eventually.
\end{itemize}
