\hypertarget{challenges}{%
\chapter{Challenges}\label{challenges}}

\colorbox{lightgray}{
  \parbox{\textwidth}{
  \textbf{This chapter covers:}

  \begin{itemize}
  \tightlist
  \item
    Overview of the current concerns of AI
  \item
    Known problems with Deep Reinforcement Learning
  \item
    Functional safety standards for deploying AI
  \end{itemize}
}}

\hfill \break


In some cases, AI and ML techniques have major advantages over the current
state-of-the-art software development \cite{neuralnetworksanddeeplearning}.
But, they could also be used with malicious intent. While this paper does not propose any authoritative answers to safety problems,
in the following Chapter an overview of some the technical research problems is given.


\hypertarget{Definition of technical safety}{%
\section{Definition of technical safety}\label{definition}}


A large scale analysis of the current risk and safety issues within AI was recently published by \citep{safetyissues}.
It gives an overview to the categories of AI techniques, and lists specific safety problems.
\citep{safetyissues} defined safety as follows.

\begin{quotation}
  [...] we define AI safety broadly, to include both risks from AI systems for which
  the source of risk is accidental, ranging from unpredictable systems to negligent use, and non-accidental
  risks such as those stemming from malicious use or adversarial attacks (which might sometimes also be referred to as AI security
  risks.) This includes risks with many different types of consequences: including human, environmental, and economic consequences.
  AI safety is becoming particularly important as AI is increasingly used to automate tasks that involve interaction with the world.
\end{quotation}

There are several safety issues with AI systems. Some of them are non-technical, such as the moral dilemma, the
utilitarian ethics problem and the choice of ethical preferences, the cognitive atrophy problem, and data privacy and GDPR violence problems.
Others are technical, such as ensuring reliability and robustness, reward hacking, or safe exploration and
negative impact problems.

In a technically oriented paper concrete problems in AI safety are listed \cite{concreteproblems},
and three major areas of technical safety are summarized in a publication by \citep{deepmindsafety}.
All these areas need to be incorporated at the various stages of system development.

\begin{itemize}
  \item \textbf{Specification} - \textit{The definition of the system's purpose.} \\
  Ensuring that the system's behaviour aligns with the developers and operators true intention is difficult.
  This is not something attributed only to AI. Bugs and inconsistencies are relevant for any programming task.
  The development process included designing the specifications and ensuring the system's
  behaviour is aligned with expectation. A necessary component of safety is the definition of a complete
  set of realistic and safe requirements. \citep{shalevshwartz2018formal} developed a model that put driving rules
  (e.g. \emph{right of way is given, not taken}) into formal mathematical statements.

  \item \textbf{Robustness} - \textit{A system design that withstands perturbations.} \\
  This area focuses on ensuring that the system continues to operate within safe limits and prevents risk or recovers into a stable state.
  Once a prototype of a system is available, safety validation can be performed through testing, performance evaluation, and interpretation
  of the failure modes of the system. Testing can discover failures due to implementation bugs, missing
  requirements, and emergent behaviour due to the complex interaction of subcomponents.
  \citep{corso2020survey} states that in complex autonomous systems operating in physical environments, safety in
  all situations cannot be guaranteed, so performance evaluation techniques can determine if the system is acceptably safe. One method
  to validate safety is treating the system as a black-box and implementing algorithms that generate
  disturbances to see the system's behaviour \cite{corso2020survey}.

  \item \textbf{Assurance} - \textit{System monitoring and activity control.} \\
  The control flow of an AI system is not directly obvious. However, techniques can be used to ensure that humans can understand and control these systems during operation.
  Among those is to visualise how the system came to its decision such a system is then called transparent or interpretable. Another technique that helps in cases of emergency
  is to add the ability to interrupt the system or require authorisation for every action it takes. To ensure data privacy, encryption can be used.
\end{itemize}


\hypertarget{problemsalgorithms}{%
\section{Algorithms}\label{problemsalgorithms}}

In the following segment, some known problems of Deep RL algorithms will be explained. As stated in Chapter \ref{preliminaries}, the \textbf{exploration
vs. exploitation} dilemma is a key component of the Deep RL problem setup. It formalises the question of how to make the most cost-effective decision with
incomplete information. With exploitation, advantage of the best known option is taken. With exploration, some risk to collect information about
unknown options is taken. In the literature, the Multi-Armed Bandit setup is often used to describe this problem \cite{Sutton1998}. To solve this, a few exploration algorithms can be learned, each with
different benefits and trade-offs. A few major themes are described by \citep{weng2018PG}:

\begin{itemize}
\tightlist
\item
  \textbf{Random algorithm} - The best known actions are used most of the time, but exploration happens randomly. This tends to converge,
  if at all, very slowly towards an optimum.
\item
  \textbf{Optimistic exploration} - The agent selects the greediest action to maximise the upper confidence bound
   $\hat{Q}_t(a) + \hat{U}_t(a)$, where $\hat{Q}_t(a)$ is the average reward associated with action $a$ up to time $t$ and $\hat{U}_t(a)$ is a
   function reversely proportional to how many times s action has been taken.
\item
  \textbf{Posterior sampling} - The agent keeps track of a belief over the probability of optimal actions and samples from this distribution
\item
  \textbf{Information gain} - Adds an entropy term $H(\pi(a \vert s))$ into the loss function, encouraging the policy to take diverse action
\end{itemize}

The \textbf{Deadly Triad Issue} is a relatively new problem in the convergence towards an optimum. It arises from the combination of
function approximation, bootstrapping and off-policy learning \cite{Sutton1998}. Given that neural networks are used to approximate the Q values of each state-action pair, errors
can happen if the value pairs are similar enough to be approximately the same. If that happens, the assumption that two state-action pairs are the same is made,
when they are not. This can lead to actions being taken in states that do not have the highest value.

Further research on core algorithms needs to be done. The problem of tuning the hundreds of parameters in a
neural network and to ensure that the algorithm is stable in its convergence is well known \cite{Goodfellow-et-al-2016}.
The sensitivity to hyperparameters leads to problems with generalization, and even if the hyperparameters
work in the simulated environment, there is no guarantee about robustness in the physical world \cite{dulacarnold2019challenges}.

The design of a reward function is also notoriously difficult \cite{concreteproblems}.
The goal is to create a function that encourages the behaviors that help and avoid all other
behaviors. While simple in explanation, in practice it is difficult.
There have been approaches to have a separate neural network that learns
the reward function itself with an additional feedback from a human \cite{christiano2017deep}.

Deep RL algorithms also tend to require a lot of samples until a good policy is found (Figure \ref{fig:eff}). While off-policy algorithms
generally need less than on-policy algorithms, both still need hundreds and thousands of episodes until convergence.
This severely limits the applicability to the real-world (Chapter \ref{experiments}).

\begin{figure}
  \centering
  \includegraphics[scale=0.5]{/Users/daniel/Library/Application Support/typora-user-images/image-20210324114543737.png}
  \caption{Sample efficiency of algorithms. Source: CS285 at UC Berkley\label{fig:eff}}
\end{figure}


\hypertarget{deployment}{%
\section{Deployment}\label{deployment}}


As stated \textbf{deploying Deep RL agents} into the real world also comes with a number of challenges \cite{dulacarnold2019challenges}.
For example, simulators may be using discrete action-spaces and state-spaces for ease of use. The real world is more complicated though, and training on discrete
values does not translate well to operating on continuous values. Issues with reward functions may again be a problem if they are unspecific or
multi-objective, e.g. a single function for both lane changing and acceleration. Additionally there may also be large and/or unknown
delays in the system actuators or sensors.

The automotive industry requires certain standards to be implemented.
One of those is the ISO26262 that provides a safety lifecycle, a development and operation
process, methods to determine risk, and other useful guidelines. Some efforts have been made to update the
development process \cite{Radlak_2020}. Although the standard does not officially cover anything
about ML techniques yet, autonomous vehicles are actively being deployed \cite{kiran2021deep}.

Nevertheless, the technical challenge of developing an autonomous system is interesting, given that multiple data
sources from all the different sensor (camera, lidar, radar, etc.), given that multiple
tasks need to be done (sensing, understanding, acting, etc.), and given that these systems need to be safely integrated
into the physical world.

In general for software the ISO26262 provides a product development process and mechanisms
for error handling. \citep{salay2017analysis} analysed how this could be applied to ML.
They stated that the use of fault detection tools and techniques need to be taken into account.
Most notably, they stated that the approach to implementing a safe software relates
to the amount of available specification. If a problem can be specified thoroughly  enough,
conventional programming may be more beneficial than ML techniques. Partial specifications
may be used for both approaches, and should be checked after training and testing. Finally
they do not recommend an end-to-end approach to autonomous driving with ML techniques.
This is applied in Chapter \ref{experiments} where a safety specification is defined
and the model's behavior is verified after training.
