\hypertarget{experiments}{%
\chapter{Experiments}\label{experiments}}

\colorbox{lightgray}{
  \parbox{\textwidth}{
  \textbf{This chapter covers:}

  \begin{itemize}
  \tightlist
  \item A comparison of value-based and actor-critic methods
  \item The implementation of a fail-safe mechanism to minimise negative side effects
  \item Interpretation of training run results
  \end{itemize}
}}

\hfill \break

ML systems are implemented in several phases. In the design phase,
the problem is formalised, the necessary data is accumulated, and an algorithm is chosen.
For Deep RL systems, a simulation or video-game is often used \cite{Mnih2015}
and most current algorithms are available through frameworks (Section \ref{relatedwork}).
In the development phase, a first prototype is implemented and evaluated. Finally, the
system is optimised and deployed.

This paper's accompanying experiment \footnote{Paper's code: https://github.com/dmelichar/thesis-code}
was conducted in similar fashion, the exception being the final phase for
optimisation and deployment. The lack of optimisation was due to time constraints,
and more so due to sample inefficiency (Chapter \ref{challenges}). A single training run where multiple
agents were compared took about four hours to complete. This made experimentation with model parameters costly.
Although parallelisation through Docker virtualisation was done to generate models, the development
experience and the time needed to see results was still frustrating. The lack of deployment was simply due to
no hardware being available where model could be deployed to.

\hypertarget{agent-design}{%
\section{Agent Design}\label{agent-design}}

Two initial agents were developed, DQN as value-based method and A2C as an actor-critic algorithm.
DQN and A2C were modified in two ways. The first modification was an external controller that
took over the agent's action choice if its state was unsafe (Figure \ref{fig:controller}).
The controller used code provided by the environment framework, and served as fail-safe mechanism to
minimise the impact of negative side effects. The second modification was an added reward for being in a
safe state, and a penalty for being in an unsafe state (Figure \ref{fig:rewardmodelling}).

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{./PICS/controller.png}
    \caption{Fail-Safe Controller}
    \label{fig:controller}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth]{./PICS/reward.png}
    \caption{Reward Modelling}
    \label{fig:rewardmodelling}
  \end{subfigure}
  \caption{Diagram of modifications}
\end{figure}

Both the value-based algorithm and the actor-critic algorithm were equipped with these
modifications, so that an overall of six algorithms were compared.
Other than the stated modifications, no changes were made to the algorithms.
They were implemented as explained in Chapter \ref{preliminaries}. A list of hyperparameters and
neural network layers is given in Attachment \ref{hyperparameters}.

The six implemented agents are as follows:

\begin{itemize}
  \item \textbf{A2CAgent} - Actor-Critic method
  \item \textbf{A2CControlAgent} - Actor-Critic method with added controller
  \item \textbf{A2CSafeAgent} - Actor-Critic method with added reward
  \item \textbf{DQNAgent} - Value-based method
  \item \textbf{DQNControlAgent} - Value-based method with added controller
  \item \textbf{DQNSafeAgent} - Value-based method with added reward
  \item \textbf{BaselineAgent} - Non-probabilistic agent based on controller
\end{itemize}

The modified environment \texttt{LunarSafe} returned the agent's safety after each transition by using
a first-order logic function based on the agent's coordinates (Section \ref{environment-design}).
After each action, and thereby each new state, the agent received the standard markovian transition tuple $(s, a, s_{t+1}, r)$
as well as a new boolean parameter $safe$. Where $s$ is the initial state, $a$ is the agent's action, $s_{t+1}$ is the next state,
and $r$ the reward.

\subsection{Fail-Safe Controller}

The core idea behind the fail-safe controller (Figure \ref{fig:controller}) is to have a
static recovery mechanism. After every action the agent observes the environment's
state. If the $safe$ parameter in the tuple at $s_t$ is set to $false$, the controller will
override the action choice. In other words, the agents action predictions will not be
used whenever it is in an unsafe state. The controller overrides the agent's action
choice until the $safe$ parameter is set to $true$ again.

The controller is setup with heuristics from the modified \texttt{LunarLander} environment.
It was possible to determine exact limits for angle and distance of the lander through experimentation.
The controller had the state $s$ as parameter where the agent's horizontal and vertical
coordinates, its horizontal and vertical speed, as well as its angle and angular speed are present.
With that information the controller calculates the best action: doing nothing,
firing the left orientation engine, firing the main engine, or firing right orientation engine.
The exact values can be found in the provided code.

\subsection{Reward Modelling}

The new parameter $safe$ was also used for reward changes. If the agent was in a safe state, it
received an additional \texttt{+10} to its reward in order to ensure that the action leading to a
safe state is repeated. If the agent was in an unsafe state, it received a penalty of \texttt{-10} to its reward,
ensuring that it does not repeat the action in a similar state again.

\hypertarget{environment-design}{%
\section{Environment Design}\label{environment-design}}

Training and evaluation was done in a modified, discrete version of \texttt{LunarLander} from the
OpenAI Gym framework \footnote{Environment framework: https://github.com/openai/gym}.
Figure \ref{fig:env} shows graphics of the simulated environment. The red bars show the
bounds of the safety specification. If they agent stayed within the red bars, i.e. within
the limits of the safety function, the agent was safe and observed the $safe$ parameter as $true$.
If they agent went outside the bounds of the safety functions, as is shown in Figure \ref{fig:env},
it was not safe and observed the $safe$ parameter as $false$.

\begin{figure}
  \centering
  \includegraphics[scale=0.5]{PICS/screenshot.png}
  \caption{Modified LunarLander environment}
  \label{fig:env}
\end{figure}

\citep{felix} stated that the safety of a dynamical system $f$ is a property of a sequence of states (or trajectories $\tau$)
visited and actions applied under a policy $\pi$, and that safety can be defined for these trajectories
with a specification - for this experiment, an enforced constraint. A single function $c$
is used to analyze a single state or trajectories, and returns a scalar value if
the trajectory or state is safe. This also fulfills the safety specifications from a
standard such as the ISO 26262.

\begin{equation}
 c(\tau) \ge 0
\end{equation}

The safety function's constraint is a limit on the coordinates of the agent.
If the agent moved outside out of the safe bound, it was deemed unsafe. The bound is
defined as 20\% from the desired landing location. If the agent remained within
these bounds, that is, if the distance between the agent and the vertical landing coordinates
is within a 20\% threshold, it was deemed safe. This specification was integrated
into the environment as safety function.

\hypertarget{Results}{%
\section{Results}\label{results}}

Overall there were three training runs. The DQN agents were trained for 1000 episodes, whereas the A2C
agents were trained for 5000 episodes. This is to reflect the known sample inefficiency
of policy gradient methods (Chapter \ref{challenges}). On average a single run took around four hours.
The resulting scores and safety plots can be found in Figures \ref{fig:scores} and Figures \ref{fig:safety} respectively.
Generally speaking all actor-critic agents did not converge to an optimum. The
A2CSafeAgent had high scores, but dropped off in two of the three runs. The
high scores are also due to the added reward. It does not correlate to performance.
The value-based agents did much better and converged to an optimum. Although
the DQNSafeAgent seemed to reach better scores than Baseline, this is again
due to the higher total reward. It does not reflect actual performance. DQNControlAgent
had the best performance, it converged quickly and stayed there. The naive
DQNAgent required an average of 700 episodes until convergence.

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/a2c-run2-scores.png}
    \caption{A2C Run 1}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/a2c-run4-scores.png}
    \caption{A2C Run 2}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/a2c-run5-scores.png}
    \caption{A2C Run 3}
  \end{subfigure}
  \\[\smallskipamount]
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/DQN-run2-scores.png}
    \caption{DQN Run 1}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/DQN-run4-scores.png}
    \caption{DQN Run 2}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/DQN-run5-scores.png}
    \caption{DQN Run 3}
  \end{subfigure}
  \caption{Training run scores}
  \label{fig:scores}
\end{figure}

\begin{figure}[!ht]
  \centering
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{./PICS/a2c-run4-safety.png}
    \caption{A2C Run 2}
  \end{subfigure}
  \begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=\linewidth]{./PICS/a2c-run5-safety.png}
    \caption{A2C Run 3}
  \end{subfigure}
  \\[\smallskipamount]
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/DQN-run2-safety.png}
    \caption{DQN Run 1}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/DQN-run4-safety.png}
    \caption{DQN Run 2}
  \end{subfigure}
  \begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=\linewidth]{./PICS/DQN-run5-safety.png}
    \caption{DQN Run 3}
  \end{subfigure}
  \caption{Training run safety}
  \label{fig:safety}
\end{figure}

In terms of safety, overall the value-based methods did better than the actor-critic methods.
The actor-critic methods had no significant changes. The value-based methods, especially
the DQNSafeAgent had significant improvements to safety. However, again, this may not
reflect actual performance. Nevertheless, the naive DQNAgent did improve as well.

Overall the results showed that the value-based methods learned better.
While there is some difference between the value-based methods, they are
reflected only in time-until-convergence. In most cases the default DQNAgent may be sufficient. If
the training domain is safety critical and formal specifications are available,
the DQNControlAgent would be the best choice.


\hypertarget{Discussion}{%
\section{Discussion}\label{discussion}}
There are a few problems with this approach. One, as stated in Chapter \ref{challenges}, is the
trade-off between simulation and the physical world. Finding the
right specification in the physical world may not be as easy. The simulation allowed
finding the bounds of the function empirically at no cost whereas this may not be the
case in the physical world.

Finding the right specification and ensuring that the agent's behavior aligns with intent
requires further research. The main issue is granularity.
If the specification is too exact, the system does not learn and conventional programming
may be better. If the specification is too unsophisticated, the agent may produce undesirable
actions.

Even if the right specification is found,
the question of ML being necessary by then rises. At its core ML is a form of optimization
under uncertainties. If a full specification can be developed, conventional programming
may be safer and transparent. But for certain problems, such as computer vision, it will be
almost impossible to define all statements and conditions to process an image and ML is
far superior.

In safe RL, a model of the system must be learned first.
In the beginning of the learning process this may lead to errors. This
is an actively researched topic \cite{felix}.  The setup of having a ML system
generating predictions with a conventionally programmed controller
ensuring that the system remains safe may be applicable. It allows for the
benefits of ML to be used, while still making sure that more rigorously defined conditions are
used safety-critical states. Additionally, since some vehicles today have components programmed with
conventional methods. The code may already exist for some problems, and some experimentation
and migrations with new methods as Deep RL or computer vision could bring benefits.

Several methods can be implemented to further optimise the agents. For the value-based
agents, different exploration strategies can be implemented, as referenced in Chapter \ref{challenges}.
For the policy gradient methods, a different optimisation technique can be used, such as
the Bayesian optimisation \cite{BerkenkampMSK16}. Other methods include automatic hyperparameter tuning,
and the development of a neural network to learn the reward function \cite{christiano2017deep}.

\clearpage
